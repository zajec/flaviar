<?php

namespace App\Controller;

use App\Service\GetDistinctRatesService;
use App\Service\GetTableReportDataService;
use App\Service\ParseHistoricalExchangeRatesService;
use App\Service\PrepareExcelFileService;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class TableReportController
 * @package App\Controller
 */
class TableReportController extends AbstractController
{
    /** @var ParseHistoricalExchangeRatesService $parseHistoricalExchangeRatesService */
    private $parseHistoricalExchangeRatesService;

    /** @var GetTableReportDataService $getTableReportDataService */
    private $getTableReportDataService;

    /** @var GetDistinctRatesService $getDistinctRatesService */
    private $getDistinctRatesService;

    /** @var PrepareExcelFileService $prepareExcelFileService */
    private $prepareExcelFileService;

    /**
     * TableReportController constructor.
     * @param ParseHistoricalExchangeRatesService $parseHistoricalExchangeRatesService
     * @param GetTableReportDataService $getTableReportDataService
     * @param GetDistinctRatesService $getDistinctRatesService
     * @param PrepareExcelFileService $prepareExcelFileService
     */
    public function __construct(
        ParseHistoricalExchangeRatesService $parseHistoricalExchangeRatesService,
        GetTableReportDataService $getTableReportDataService,
        GetDistinctRatesService $getDistinctRatesService,
        PrepareExcelFileService $prepareExcelFileService
    )
    {
        $this->parseHistoricalExchangeRatesService = $parseHistoricalExchangeRatesService;
        $this->getTableReportDataService = $getTableReportDataService;
        $this->getDistinctRatesService = $getDistinctRatesService;
        $this->prepareExcelFileService = $prepareExcelFileService;
    }

    /**
     * @return Response
     */
    public function home(): Response
    {
        return $this->render('home.html.twig');
    }

    /**
     * @return Response
     * @throws Exception
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getExchangeRates(): Response
    {
        $exchangeRates = $this->parseHistoricalExchangeRatesService->parse();

        return $this->render('home.html.twig', ['exchange_rates' => $exchangeRates['rates']]);
    }

    /**
     * @return Response
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    public function tableReport(): Response
    {
        $year = 2020;
        $data = $this->getTableReportDataService->get();

        return $this->render('table-report.html.twig', compact('year', 'data'));
    }

    /**
     * @return BinaryFileResponse
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    public function exportExcel(): BinaryFileResponse
    {
        $fileName = 'Exchange rates for year 2020.xlsx';

        $this->prepareExcelFileService->prepare($fileName, $this->getTableReportDataService->get());

        return new BinaryFileResponse($fileName);
    }

    /**
     * @return Response
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    public function distinctRates(): Response
    {
        $data = $this->getDistinctRatesService->get();

        return $this->render('distinct-report.html.twig', compact('data'));
    }

}