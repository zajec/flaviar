<?php

namespace App\Controller\Api;

use App\Service\GetExchangeRateByDateService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExchangeRatesController
 * @package App\Controller
 * @Route("/api/exchange-rates", name="exchange rates")
 */
class ExchangeRatesController extends AbstractController
{
    /** @var GetExchangeRateByDateService $getExchangeRateByDateService */
    private $getExchangeRateByDateService;

    /**
     * ExchangeRatesController constructor.
     * @param GetExchangeRateByDateService $getExchangeRateByDateService
     */
    public function __construct(GetExchangeRateByDateService $getExchangeRateByDateService)
    {
        $this->getExchangeRateByDateService = $getExchangeRateByDateService;
    }

    /**
     * @Route("/exchange-rate", name="exchange rate")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function getExchangeRateForDate(Request $request): JsonResponse
    {
        $date = $request->get('date');

        if (is_null($date) || empty($date)) {
            throw new Exception('Parameter date is not defined');
        }

        $exchangeRate = $this->getExchangeRateByDateService->get($date);

        if ($exchangeRate === false) {
            throw new Exception('Exchange rate for provided date is not available.');
        }

        return $this->json(['exchange_rate', $exchangeRate]);
    }
}