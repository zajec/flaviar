<?php

namespace App\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;

/**
 * Class GetDistinctRatesService
 * @package App\Service
 */
class GetDistinctRatesService
{
    /** @var Connection $dbConnection */
    private $dbConnection;

    /**
     * GetDistinctRatesService constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->dbConnection = $db;
    }

    /**
     * @return array[]
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    public function get()
    {
        return $this->dbConnection->createQueryBuilder()
            ->select('rate, GROUP_CONCAT(date ORDER BY date ASC) as dates')
            ->from('exchange_rates')
            ->groupBy('rate')
            ->orderBy('rate', 'ASC')
            ->execute()
            ->fetchAllAssociative();
    }
}