<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class PrepareExcelFileService
{
    private $spreadsheet;

    public function __construct()
    {
        $this->spreadsheet = new Spreadsheet();
    }

    public function prepare(string $fileName, array $data)
    {
        $sheet = $this->spreadsheet->getActiveSheet();

        $sheet->setTitle('Exchange rates for year 2020');

        $sheet->getCell('A1')->setValue('Month');
        $sheet->getCell('B1')->setValue('Min rate')->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
        $sheet->getCell('C1')->setValue('Max rate')->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
        $sheet->getCell('D1')->setValue('Average rate')->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);

        $sheet->fromArray($data, null, 'A2', true);

        $writer = new Xlsx($this->spreadsheet);
        $writer->save($fileName);
    }
}