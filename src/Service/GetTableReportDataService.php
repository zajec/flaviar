<?php

namespace App\Service;

use App\Constants\ExchangeRatesConstants;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;

/**
 * Class GetTableReportDataService
 * @package App\Service
 */
class GetTableReportDataService
{
    /** @var Connection $dbConnection */
    private $dbConnection;

    /**
     * GetTableReportDataService constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->dbConnection = $db;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    public function get(): array
    {
        $data = [];

        foreach (ExchangeRatesConstants::MONTHS as $month) {
            $data[$month] = $this->dbConnection->createQueryBuilder()
                ->select('monthname(`date`) as month_name, MIN(rate) as min_rate, MAX(rate) as max_rate, CAST(AVG(rate) as DECIMAL(7,6)) as avg_rate')
                ->from('exchange_rates')
                ->andWhere(sprintf('monthname(`date`) = "%s"', $month))
                ->execute()
                ->fetchAssociative();
        }

        return $data;
    }
}