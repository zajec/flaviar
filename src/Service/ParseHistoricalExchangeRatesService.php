<?php

namespace App\Service;

use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Doctrine\DBAL\Connection;

class ParseHistoricalExchangeRatesService
{
    private $client;
    private $dbConnection;

    public function __construct(HttpClientInterface $client, Connection $db)
    {
        $this->client = $client;
        $this->dbConnection = $db;
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws \Doctrine\DBAL\Exception
     */
    public function parse(): array
    {
        $startDate = '2020-01-01';
        $endDate = '2020-12-31';
        $symbols = 'USD';

        $url = sprintf(
            'https://api.exchangeratesapi.io/v1/timeseries?access_key=%s&start_date=%s&end_date=%s&symbols=%s&format=1',
            'bbd8a2ce32adcba1c6c65e20acd33306', //TODO: use getenv('EXCHANGE_RATES_API_ACCESS_KEY'),
            $startDate,
            $endDate,
            $symbols
        );

        $response = $this->client->request(
            'GET',
            $url
        );

        if ($response->getStatusCode() !== 200) {
            throw new Exception('Can not get exchange rates from API ' . $response->getStatusCode());
        }

        $rates = $response->toArray()['rates'];

        if (!empty($rates)) {
            $this->dbConnection->executeStatement('TRUNCATE exchange_rates;');
        }

        foreach ($rates as $date => $value) {
            $this->dbConnection->insert(
                'exchange_rates', [
                    'date' => $date,
                    'rate' => $value['USD'],
                ]
            );
        }

        return $response->toArray();
    }
}