<?php

namespace App\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;

/**
 * Class GetExchangeRateByDateService
 * @package App\Service
 */
class GetExchangeRateByDateService
{
    /** @var Connection $dbConnection */
    private $dbConnection;

    /**
     * GetExchangeRateByDateService constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->dbConnection = $db;
    }

    /**
     * @param string $date
     * @return false|mixed
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    public function get(string $date)
    {
        return $this->dbConnection->createQueryBuilder()
            ->select('rate')
            ->from('exchange_rates')
            ->andWhere('date = ' . $date)
            ->execute()
            ->fetchOne();
    }
}