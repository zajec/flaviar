<?php

namespace App\Command;

use App\Service\ParseHistoricalExchangeRatesService;
use Doctrine\DBAL\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ParseHistoricalExchangeRatesCommand
 * @package App\Command
 */
class ParseHistoricalExchangeRatesCommand extends Command
{
    /** @var ParseHistoricalExchangeRatesService $parseHistoricalExchangeRatesService */
    private $parseHistoricalExchangeRatesService;

    /**
     * ParseHistoricalExchangeRatesCommand constructor.
     * @param string|null $name
     * @param ParseHistoricalExchangeRatesService $parseHistoricalExchangeRatesService
     */
    public function __construct(string $name = null, ParseHistoricalExchangeRatesService $parseHistoricalExchangeRatesService)
    {
        parent::__construct($name);
        $this->parseHistoricalExchangeRatesService = $parseHistoricalExchangeRatesService;
    }

    protected function configure()
    {
        $this
            ->setName('flaviar:exchange-rates:parse')
            ->setDescription('Parse exchange rates');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->parseHistoricalExchangeRatesService->parse();

        return Command::SUCCESS;
    }
}