<?php

namespace App\Constants;

/**
 * Class ExchangeRatesConstants
 * @package App\Constants
 */
class ExchangeRatesConstants
{
    const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
}