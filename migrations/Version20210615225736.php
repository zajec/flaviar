<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210615225736 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'CREATE TABLE `exchange_rates` (
                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                    `date` DATE NOT NULL,
                    `rate` DECIMAL(7,6) NOT NULL,
                    PRIMARY KEY (`id`)
                );'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE `exchange_rates`');

    }
}
